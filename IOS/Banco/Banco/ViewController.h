//
//  ViewController.h
//  Banco
//
//  Created by jaldeano on 12/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UILabel *label;

- (IBAction)slider:(id)sender;
- (IBAction)doneKeyboard:(id)sender;
- (IBAction)backgroundClick:(id)sender;

@end

