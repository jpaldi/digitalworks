//
//  ViewController.m
//  Banco
//
//  Created by jaldeano on 12/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.label.text = @"0";
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)slider:(id)sender {
    UISlider *slider = (UISlider*) sender;
    int val = (int) slider.value;
    self.label.text = [[NSString alloc] initWithFormat:@"%i", val];
}

- (IBAction)doneKeyboard:(id)sender {
    [self resignFirstResponder];
}

- (IBAction)backgroundClick:(id)sender {
    [self.name resignFirstResponder];
    [self.password resignFirstResponder];
}

- (void) setBackground{
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"ima.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
}
@end
