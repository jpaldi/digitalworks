//
//  ViewController.h
//  Calculadora
//
//  Created by jaldeano on 12/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *numberOne;
@property (weak, nonatomic) IBOutlet UISegmentedControl *operationChoice;
@property (weak, nonatomic) IBOutlet UITextField *numberTwo;
@property (weak, nonatomic) IBOutlet UILabel *result;

- (IBAction)calculate:(id)sender;
- (IBAction)clear:(id)sender;
- (IBAction)doneKeyboard:(id)sender;
- (IBAction)clickBackground:(id)sender;
- (void)divideByZeroAlert;
@end

