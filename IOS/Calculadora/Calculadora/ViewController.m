//
//  ViewController.m
//  Calculadora
//
//  Created by jaldeano on 12/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bground.png"]];
    [self.result setText:@""];
    self.numberOne.text = @"0";
    self.numberTwo.text = @"0";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)calculate:(id)sender {
    
    if ([self isDecimal:self.numberOne.text] && [self isDecimal:self.numberTwo.text]){
        
        float num1 = [self.numberOne.text floatValue];
        float num2 = [self.numberTwo.text floatValue];
        
        float res;
        NSInteger op = [self.operationChoice selectedSegmentIndex];
        
        switch (op) {
            case 0:
                res = num1 + num2;
                break;
                
            case 1:
                res = num1 - num2;
                break;
                
            case 2:
                res = num1 * num2;
                break;
                
            case 3:
                if ([self.numberTwo.text isEqualToString:@"0"]){
                    [self divideByZeroAlert];
                    return;
                }
                
                else{
                    res = num1 / num2;
                    [self.result setText:@""];
                    break;
                }
                
            default:
                break;
        }
        
        NSString * result = [[NSString alloc] initWithFormat:@"%0.2f",res];
        
        [self.result setText:[NSString stringWithFormat:@"Result: %@", result] ];
        
    }
    else{
        [self notDecimalAlert];
    }
}

- (IBAction)clear:(id)sender {
    self.numberOne.text = nil;
    self.numberTwo.text = nil;
    self.result.text = nil;
}

- (IBAction)doneKeyboard:(id)sender{
    [self resignFirstResponder];
}

- (IBAction)clickBackground:(id)sender{
    [self.numberOne resignFirstResponder];
    [self.numberTwo resignFirstResponder];

}

- (void)divideByZeroAlert{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Divide by Zero"
                                                    message:@"You can't divide a number by zero."
                                                   delegate:self
                                          cancelButtonTitle:@"Exit"
                                          otherButtonTitles:nil];
    [alert show];
    self.numberOne.text = nil;
    self.numberTwo.text = nil;
}

- (BOOL) isDecimal: (NSString*) str{
    NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
    BOOL isDecimal = [nf numberFromString:str] != nil;
    
    if (isDecimal){
        return YES;
    }
    else{
        return NO;
    }
    
}


- (void)notDecimalAlert{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid Numbers"
                                                    message:@"Insert valid numbers"
                                                   delegate:self
                                          cancelButtonTitle:@"Exit"
                                          otherButtonTitles:nil];
    [alert show];
    self.numberOne.text = nil;
    self.numberTwo.text = nil;
}

@end
