//
//  AppDelegate.m
//  FootPic Quizz
//
//  Created by jaldeano on 29/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "AppDelegate.h"
#import "MainMenuViewController.h"
static int questionDataSize = 11;

@interface AppDelegate ()
@property (strong, nonatomic) UINavigationController *navigationController;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [self delete_all_data];
    [self read_data_file];
    
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:[[MainMenuViewController alloc] initWithNibName:@"MainMenuViewController" bundle:nil]];
    self.navigationController.navigationBarHidden = YES;
    [self.window makeKeyAndVisible];
    self.window.rootViewController = self.navigationController;
    return YES;
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.digitalworks.com.FootPic_Quizz" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"FootPic_Quizz" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"FootPic_Quizz.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (void) delete_all_data{
    NSPersistentStore * store = [[self.persistentStoreCoordinator persistentStores] lastObject];
    NSError * error;
    [self.persistentStoreCoordinator removePersistentStore:store error:&error];
    [[NSFileManager defaultManager] removeItemAtURL:[store URL] error:&error];
    _managedObjectContext = nil;
    _persistentStoreCoordinator = nil;
    [self managedObjectContext];//Rebuild The CoreData Stack
}

- (void) read_data_file{
    NSString* filePath = @"list";
    NSString* fileRoot = [[NSBundle mainBundle]
                          pathForResource:filePath ofType:@"txt"];
    
    NSString* fileContents =
    [NSString stringWithContentsOfFile:fileRoot
                              encoding:NSUTF8StringEncoding error:nil];
    // first, separate by new line
    NSArray* allLinedStrings =
    [fileContents componentsSeparatedByCharactersInSet:
     [NSCharacterSet newlineCharacterSet]];
    
    NSString *newID = @"";
    NSString *question = @"";
    NSString *answer1 = @"";
    NSString *answer2 = @"";
    NSString *answer3 = @"";
    NSString *answer4 = @"";
    NSString *right_answer = @"";
    NSString *league = @"";
    NSString *image = @"";
    NSString *difficulty = @"";
    
    for (int i = 0; i < allLinedStrings.count ; i++){
        
        if (i % questionDataSize == 0){
            newID = [allLinedStrings objectAtIndex:i];
        }
        
        else if ( i % questionDataSize == 1 ){
            question = [allLinedStrings objectAtIndex:i];
        }
        
        else if ( i % questionDataSize == 2){
            answer1 = [allLinedStrings objectAtIndex:i];
        }
        
        else if ( i % questionDataSize == 3){
            answer2 = [allLinedStrings objectAtIndex:i];
        }
        
        else if ( i % questionDataSize == 4){
            answer3 = [allLinedStrings objectAtIndex:i];
        }
        else if ( i % questionDataSize == 5){
            answer4 = [allLinedStrings objectAtIndex:i];
        }
        else if ( i % questionDataSize == 6){
            right_answer = [allLinedStrings objectAtIndex:i];
        }
        else if ( i % questionDataSize == 7){
            image = [allLinedStrings objectAtIndex:i];
        }
        else if ( i % questionDataSize == 8){
            league = [allLinedStrings objectAtIndex:i];
        }
        else if ( i % questionDataSize == 9){
            difficulty = [allLinedStrings objectAtIndex:i];
        }
        
        else if ( i % questionDataSize == 10){
            
            // INSERT CORE DATA
            NSManagedObjectContext *context = [self managedObjectContext];
            
            NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Question"];
            NSError *error = nil;
            [request setPredicate:[NSPredicate predicateWithFormat:@"id = %@", newID]];
            [request setFetchLimit:1];
            NSUInteger count = [context countForFetchRequest:request error:&error];
            if (count == NSNotFound){
                // some error occurred
            }
            else if (count == 0) {
                // Create a new managed object
                NSManagedObject *newQuestion = [NSEntityDescription insertNewObjectForEntityForName:@"Question" inManagedObjectContext:context];
                
                // no matching object
                NSLog(@"vai criar um objecto com id: %@", newID);
                [newQuestion setValue:newID forKey:@"id"];
                [newQuestion setValue:answer1 forKey:@"answer1"];
                [newQuestion setValue:answer2 forKey:@"answer2"];
                [newQuestion setValue:answer3 forKey:@"answer3"];
                [newQuestion setValue:answer4 forKey:@"answer4"];
                [newQuestion setValue:question forKey:@"question"];
                [newQuestion setValue:image forKey:@"image"];
                [newQuestion setValue:league forKey:@"league"];
                [newQuestion setValue:difficulty forKey:@"difficulty"];
                [newQuestion setValue:right_answer forKey:@"right_answer"];

            }
            else{
                // at least one matching object exists
                NSFetchRequest *request = [[NSFetchRequest alloc] init];
                [request setEntity:[NSEntityDescription entityForName:@"Question" inManagedObjectContext:context]];
                ;
                
                NSArray *results = [context executeFetchRequest:request error:&error];
                
                NSManagedObject *questionGrabbed = [results objectAtIndex:0];
                [questionGrabbed setValue:newID forKey:@"id"];
                [questionGrabbed setValue:answer1 forKey:@"answer1"];
                [questionGrabbed setValue:answer2 forKey:@"answer2"];
                [questionGrabbed setValue:answer3 forKey:@"answer3"];
                [questionGrabbed setValue:answer4 forKey:@"answer4"];
                [questionGrabbed setValue:question forKey:@"question"];
                [questionGrabbed setValue:image forKey:@"image"];
                [questionGrabbed setValue:league forKey:@"league"];
                [questionGrabbed setValue:difficulty forKey:@"difficulty"];
                [questionGrabbed setValue:right_answer forKey:@"right_answer"];
                NSLog(@"já existe um objecto com id: %@", newID);
            }
            
            // Save the object to persistent store
            if (![context save:&error]) {
                NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
            }
            
        }
    }
}
@end
