//
//  MainMenuViewController.h
//  FootPic Quizz
//
//  Created by jaldeano on 29/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainMenuViewController : UIViewController
- (IBAction)normalGameModeAction:(id)sender;
@end
