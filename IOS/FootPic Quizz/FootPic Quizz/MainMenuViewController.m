//
//  MainMenuViewController.m
//  FootPic Quizz
//
//  Created by jaldeano on 29/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "MainMenuViewController.h"
#import "NormalGameViewController.h"

@interface MainMenuViewController ()
@end

@implementation MainMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)normalGameModeAction:(id)sender {
    NormalGameViewController *vc = [[NormalGameViewController alloc] initWithNibName:@"NormalGameViewController" bundle:nil];
    [[self navigationController] pushViewController:vc animated:YES];
}
@end
