//
//  NormalGameViewController.h
//  FootPic Quizz
//
//  Created by jaldeano on 30/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NormalGameViewController : UIViewController
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *countryTextField;
- (IBAction)goBackAction:(id)sender;
@end
