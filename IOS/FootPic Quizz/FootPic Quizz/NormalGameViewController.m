//
//  NormalGameViewController.m
//  FootPic Quizz
//
//  Created by jaldeano on 30/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "NormalGameViewController.h"
#import "PlayNormalViewController.h"
@interface NormalGameViewController ()
@end

@implementation NormalGameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sliderValueChanged:(id)sender
{
    // Set the label text to the value of the slider as it changes
    if(self.slider.value >0 && self.slider.value <1){
            self.levelLabel.text = @"Inatel";
    }
    else if (self.slider.value >= 1 && self.slider.value <2){
        self.levelLabel.text = @"Semi-Profissional";
    }
    
    else if (self.slider.value >= 2 && self.slider.value <3){
        self.levelLabel.text = @"Profissional";
    }
    else if (self.slider.value >= 3 && self.slider.value <4){
        self.levelLabel.text = @"Champions League";
    }
}

- (IBAction)goBackAction:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (IBAction)playButtonAction:(id)sender {
    PlayNormalViewController *vc = [[PlayNormalViewController alloc] initWithNibName:@"PlayNormalViewController" bundle:nil];
    [[self navigationController] pushViewController:vc animated:YES];
}
@end
