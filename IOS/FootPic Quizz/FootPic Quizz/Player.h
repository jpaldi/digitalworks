//
//  Player.h
//  FootPic Quizz
//
//  Created by jaldeano on 30/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface Player : NSObject
@property NSString *username;
@property NSString *country;
@property NSInteger score;
- (Player *) initPlayer: (NSString*)n :(NSInteger) s : (NSString*)c;
- (void) setHighscore : (NSInteger) n;
@end

