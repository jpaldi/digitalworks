//
//  Player.m
//  FootPic Quizz
//
//  Created by jaldeano on 30/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "Player.h"

@implementation Player
- (Player *) initPlayer: (NSString*)n :(NSInteger) s :(NSString*)c{
    self = [super init];
    if (self) {
        self.username = n;
        self.score = s;
        self.country = c;
    }
    return self;
}

- (NSString *) toString {
    NSString * ret = [[NSString alloc] initWithFormat:@""];
    
    ret = [NSString stringWithFormat:@"%@ from %@ : %li points", self.username, self.country ,(long)self.score];
    
    return ret;
}

- (void) setHighscore : (NSInteger) n{
    self.score = n;
}

@end
