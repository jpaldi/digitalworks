//
//  Question.h
//  FootPic Quizz
//
//  Created by jaldeano on 30/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Question : NSObject
{
    NSString *question;
    NSString *answer1;
    NSString *answer2;
    NSString *answer3;
    NSString *answer4;
    int right_answer;
    int difficulty;
    int league;
    int ident;
    NSString *image_url;
}

- (id) init :(NSString*) quest : (NSString*) ans1 : (NSString*) ans2 : (NSString*) ans3 : (NSString*) ans4 : (NSString*) ima : (int) right_ans : (int) idt : (int) diff : (int) leag;

- (int) getRightAnswer;
- (NSString *) getQuestion;
- (NSString *) getAnswer1;
- (NSString *) getAnswer2;
- (NSString *) getAnswer3;
- (NSString *) getAnswer4;
- (NSString *) getImage;
- (int) getDifficulty;
- (int) getID;
@end
