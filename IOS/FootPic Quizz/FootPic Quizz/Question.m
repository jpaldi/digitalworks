//
//  Question.m
//  FootPic Quizz
//
//  Created by jaldeano on 30/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "Question.h"

@implementation Question

- (id) init :(NSString*) quest : (NSString*) ans1 : (NSString*) ans2 : (NSString*) ans3 : (NSString*) ans4 : (NSString*) ima : (int) right_ans : (int) idt : (int) diff : (int) leag{
    if ((self = [super init])) {
        question = quest;
        answer1 = ans1;
        answer2 = ans2;
        answer3 = ans3;
        answer4 = ans4;
        league = leag;
        right_answer = right_ans;
        image_url = ima;
        ident = idt;
        difficulty = diff;
    }
    return self;
}

- (int) getRightAnswer{
    return right_answer;
}

- (NSString *) getQuestion{
    return question;
}

- (NSString *) getAnswer1{
    return answer1;
}

- (NSString *) getAnswer2{
    return answer2;
}

- (NSString *) getAnswer3{
    return answer3;
}

- (NSString *) getAnswer4{
    return answer4;
}

- (NSString *) getImage{
    return image_url;
}

- (int) getDifficulty{
    return difficulty;
}

- (int) getID{
    return ident;
}
@end
