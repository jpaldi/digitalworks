//
//  PuzzleViewController.h
//  JigSawPuzzle
//
//  Created by jaldeano on 14/10/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PuzzleViewController : UIViewController{
    UIImage *image;
    NSArray *a;
    UIPanGestureRecognizer *panner;
}
- (void) setImg : (UIImage *) img;
@end
