//
//  PuzzleViewController.m
//  JigSawPuzzle
//
//  Created by jaldeano on 14/10/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "PuzzleViewController.h"
#import "Draggable.h"
@interface PuzzleViewController ()

@end

@implementation PuzzleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self->panner = [[UIPanGestureRecognizer alloc]
                    initWithTarget:self action:@selector(panWasRecognized:)];
    [self putImagesonView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+(NSArray *)splitImageInTo9:(UIImage *)im{
    CGSize size = [im size];
    NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:9];
    for (int i=0;i<3;i++){
        for (int j=0;j<3;j++){
            CGRect portion = CGRectMake(i * size.width/3.0, j * size.height/3.0, size.width/3.0, size.height/3.0);
            UIGraphicsBeginImageContext(portion.size);
            CGContextRef context = UIGraphicsGetCurrentContext();
            CGContextScaleCTM(context, 1.0, -1.0);
            CGContextTranslateCTM(context, 0, -portion.size.height);
            CGContextTranslateCTM(context, -portion.origin.x, -portion.origin.y);
            CGContextDrawImage(context,CGRectMake(0.0, 0.0,size.width,  size.height), im.CGImage);
            [arr addObject:UIGraphicsGetImageFromCurrentImageContext()];
            UIGraphicsEndImageContext();
        }
    }
    return arr;
    
}

- (void) setImg : (UIImage *) img{
    self->image = img;
}

- (void) putImagesonView {
    self->a = [PuzzleViewController splitImageInTo9:self->image];
    int posX = 20;
    int posY = 40;
    for (int i = 0; i<sizeof(a)+1 ; i++){
        //UIImageView *imageHolder = [[UIImageView alloc] initWithFrame:CGRectMake(posX , posY, 100, 100)];
        UIImage *im = [a objectAtIndex:i];
        //CGSize s = CGSizeMake(20, 20);
        
        //imageHolder.image = [self imageWithImage:im convertToSize:s];
        // optional:
        //UIView *x = [[UIView alloc] initWithFrame:CGRectMake(posX , posY, 100, 100)];;
        //[x addSubview:imageHolder];
        //[self.view addSubview:x];
        //[x addGestureRecognizer:panner];
        //posX += 80;
        //posY += 60;
        

        
        CGRect cellRectangle = CGRectMake(posX , posY, 100, 100);
        UIImageView *dragger = [[Draggable alloc] initWithFrame:cellRectangle];
        [dragger setImage:im];
        [dragger setUserInteractionEnabled:YES];
        
        [self.view addSubview:dragger];
    }
}

- (UIImage *)imageWithImage:(UIImage *)i convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [i drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

- (void)panWasRecognized:(UIPanGestureRecognizer *)panner {
    UIView *draggedView = self->panner.view;
    CGPoint offset = [self->panner translationInView:draggedView.superview];
    CGPoint center = draggedView.center;
    draggedView.center = CGPointMake(center.x + offset.x, center.y + offset.y);
    
    // Reset translation to zero so on the next `panWasRecognized:` message, the
    // translation will just be the additional movement of the touch since now.
    [self->panner setTranslation:CGPointZero inView:draggedView.superview];
}
@end
