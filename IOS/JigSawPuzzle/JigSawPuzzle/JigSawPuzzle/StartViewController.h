//
//  StartViewController.h
//  JigSawPuzzle
//
//  Created by jaldeano on 14/10/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StartViewController : UIViewController <UINavigationControllerDelegate,
UIImagePickerControllerDelegate>
{
    BOOL picInserted;
}
@property (weak, nonatomic) IBOutlet UITextField *numberTextField;
@property (weak, nonatomic) IBOutlet UIImageView *selectedImage;

- (IBAction)touchBackground:(id)sender;
- (IBAction)nextButtonAction:(id)sender;
- (IBAction)selectPictureButtonAction:(id)sender;

@end
