//
//  StartViewController.m
//  JigSawPuzzle
//
//  Created by jaldeano on 14/10/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "StartViewController.h"
#import "PuzzleViewController.h"
@interface StartViewController ()

@end

@implementation StartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self->picInserted = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)selectPictureButtonAction:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:nil];

}

// for output image
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    // output image
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.selectedImage.image = chosenImage;
    //self.loadPictureButton.hidden = YES;
    [picker dismissViewControllerAnimated:YES completion:nil];
    self->picInserted = YES;
    
}
- (IBAction)touchBackground:(id)sender {
    [self.numberTextField resignFirstResponder];
}

- (IBAction)nextButtonAction:(id)sender {
    PuzzleViewController *vc = [[PuzzleViewController alloc] initWithNibName:@"PuzzleViewController" bundle:nil];
    [vc setImg:self.selectedImage.image];
    [[self navigationController] pushViewController:vc animated:YES];
}
@end
