//
//  MainViewController.m
//  MyApp
//
//  Created by jaldeano on 24/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *menuImageView;
@property (weak, nonatomic) IBOutlet UITextView *menuTextView;
- (IBAction)cavaquinhoButtonAction:(id)sender;
- (IBAction)campaButtonAction:(id)sender;
- (IBAction)viola1ButtonAction:(id)sender;
- (IBAction)viola2ButtonAction:(id)sender;
- (IBAction)liveActButtonAction:(id)sender;
- (IBAction)clickBackground:(id)sender;

- (IBAction)accordionButtonAction:(id)sender;
- (IBAction)exitButtonAction:(id)sender;
@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
    
    //initial picture and text
    self.menuImageView.image = [UIImage imageNamed:@"meninos.png"];
    self.menuTextView.text = @" Etiam posuere quam ac quam. Maecenas aliquet accumsan leo. Nullam dapibus fermentum ipsum. Etiam quis quam. Integer lacinia. Nulla est. Nulla turpis magna, cursus sit amet, suscipit a, interdum id, felis. Integer vulputate sem a nibh rutrum consequat. Maecenas lorem. Pellentesque pretium lectus id turpis. Etiam sapien elit, consequat eget, tristique non, venenatis quis, ante. Fusce wisi. Phasellus faucibus molestie nisl. Fusce eget urna. Curabitur vitae diam non enim vestibulum interdum. Nulla quis diam. Ut tempus purus at lorem.";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cavaquinhoButtonAction:(id)sender {
    self.menuImageView.image = [UIImage imageNamed:@"serra.png"];
    self.menuTextView.text = @"Breve descrição sobre o Serra.";
}

- (IBAction)campaButtonAction:(id)sender {
    self.menuImageView.image = [UIImage imageNamed:@"tasa.png"];
    self.menuTextView.text = @"Breve descrição sobre o Tasanis.";
}

- (IBAction)viola1ButtonAction:(id)sender {
    self.menuImageView.image = [UIImage imageNamed:@"zeca.png"];
    self.menuTextView.text = @"Breve descrição sobre o Zé Carlos.";
}

- (IBAction)viola2ButtonAction:(id)sender {
    self.menuImageView.image = [UIImage imageNamed:@"eu.png"];
    self.menuTextView.text = @"Breve descrição sobre o Aldi.";
}

- (IBAction)liveActButtonAction:(id)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Próximos concertos"
                                                                             message:@"25 Agosto - Hotel Rural Quinta do Marco (Tavira) \n \n 26 Agosto - Hotel do Cerro (Albufeira) \n \n 11 Setembro - Festas do povo (Azaruja)"
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    //We add buttons to the alert controller by creating UIAlertActions:
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                       style:UIAlertActionStyleDefault
                                                     handler:nil]; //You can use a block here to handle a press on this button
    [alertController addAction:actionOk];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)clickBackground:(id)sender {
    [self viewDidLoad];
}

- (IBAction)accordionButtonAction:(id)sender {
    self.menuImageView.image = [UIImage imageNamed:@"sandro.png"];
    self.menuTextView.text = @"Breve descrição sobre o Sandro.";
}

- (IBAction)exitButtonAction:(id)sender {
    exit(0);
}
@end
