//
//  DeviceDetailViewController.h
//  MyStore
//
//  Created by jaldeano on 19/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <CoreData/CoreData.h>
@interface DeviceDetailViewController : UIViewController
{
    NSString *name;
    NSString *version;
    NSString *company;
}

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *versionTextField;
@property (weak, nonatomic) IBOutlet UITextField *companyTextField;
@property (weak, nonatomic) IBOutlet UIButton *saveButtonVar;
@property (weak, nonatomic) IBOutlet UIButton *editButtonVar;
- (void) setEditMode : (bool) val;
- (void) setTFStrings : (NSString *) n : (NSString *) v : (NSString *) c;
- (void) setDevice : (NSManagedObject *) dev;
@end
