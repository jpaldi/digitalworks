//
//  DeviceDetailViewController.m
//  MyStore
//
//  Created by jaldeano on 19/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "DeviceDetailViewController.h"
#include <CoreData/CoreData.h>
@interface DeviceDetailViewController ()
{
    BOOL edit_mode;
    NSManagedObject *device;
}
- (IBAction)goBackButton:(id)sender;
- (IBAction)saveButton:(id)sender;
- (IBAction)editButtonAction:(id)sender;
@end

@implementation DeviceDetailViewController

- (void)viewDidLoad {
        [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (edit_mode){
        [_saveButtonVar setHidden:YES];
        [_editButtonVar setHidden:NO];
        [self.nameTextField setText:name];
        [self.versionTextField setText:version];
        [self.companyTextField setText:company];
        NSLog(@"edit mode");
    }
    else{
        [_saveButtonVar setHidden:NO];
        [_editButtonVar setHidden:YES];
        NSLog(@"add mode");
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}


- (IBAction)goBackButton:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)saveButton:(id)sender {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Create a new managed object
    NSManagedObject *newDevice = [NSEntityDescription insertNewObjectForEntityForName:@"Device" inManagedObjectContext:context];
    [newDevice setValue:self.nameTextField.text forKey:@"name"];
    [newDevice setValue:self.versionTextField.text forKey:@"version"];
    [newDevice setValue:self.companyTextField.text forKey:@"company"];
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)editButtonAction:(id)sender {
    NSManagedObjectContext *context = [self managedObjectContext];
    [device setValue:self.nameTextField.text forKey:@"name"];
    [device setValue:self.versionTextField.text forKey:@"version"];
    [device setValue:self.companyTextField.text forKey:@"company"];
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    
    [self.navigationController popToRootViewControllerAnimated:YES];

}

- (void) setEditMode : (bool) val {
    edit_mode = val;
}

- (void) setTFStrings : (NSString *) n : (NSString *) v : (NSString *) c{
    name = n;
    version = v;
    company = c;
}

- (void) setDevice : (NSManagedObject *) dev {
    device = dev;
}
@end
