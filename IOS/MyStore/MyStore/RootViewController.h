//
//  RootViewController.h
//  MyStore
//
//  Created by jaldeano on 19/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@end
