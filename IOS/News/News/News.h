//
//  News.h
//  News
//
//  Created by jaldeano on 22/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface News : NSObject
- (News *) initNews : (NSString*)t : (NSString*)l :  (NSString*)d : (NSString*) desc;


- (NSString *) getTitle;

- (NSString *) getLink;

- (NSString *) getdata;

- (NSString *) getDescription;
@end
