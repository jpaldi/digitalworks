//
//  News.m
//  News
//
//  Created by jaldeano on 22/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "News.h"

@implementation News
{
    NSString *title;
    NSString *str_link;
    NSString *data;
    NSString *description;
}

- (News *) initNews: (NSString*)t : (NSString*)l :  (NSString*)d : (NSString*) desc{
    self = [super init];
    if (self) {
        title = t;
        str_link = l;
        data = d;
        description = desc;
    }
    return self;
}

- (NSString *) getTitle{
    return title;
}

- (NSString *) getLink{
    return str_link;
}

- (NSString *) getdata{
    return data;
}

- (NSString *) getDescription{
    return description;
}
@end
