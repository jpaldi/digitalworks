//
//  NewsViewController.m
//  News
//
//  Created by jaldeano on 22/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "NewsViewController.h"
#import "News.h"
#import "ShowNewsViewController.h"
#import "News.h"
@interface NewsViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong) NSMutableArray *news;
@end

@implementation NewsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.news = [[NSMutableArray alloc] initWithCapacity:100];
    [self getJSON_Data];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// returns the number of highscores
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.news count];
}


// Loads the Core Data to TableView
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    News *new = [self.news objectAtIndex:indexPath.row];
    cell.textLabel.text = [new getTitle];
    
    // add image to cell view
    NSURL *aURL = [NSURL URLWithString:[[new getLink] stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
    
    UIImage *pic = [UIImage imageWithData:[NSData dataWithContentsOfURL:aURL]];
    
    cell.imageView.image = pic;

    //NSLog(@"%@", [new getTitle]);

    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (void) addNews: (News*)n{
    [self.news addObject:n];
}

//
- (void) getJSON_Data{
    // URL REQUEST
    NSURL *url = [NSURL URLWithString:@"http://golftattoo.com/pt/?option=com_artigos&view=rss&type=json"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             NSError *myError = nil;
             NSMutableArray *res = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&myError];
             //NSLog(@"%@",res);
             
             // show all values
             if(res!=nil)
             {
                 for(NSMutableDictionary *dict in res)
                 {
                     // get json data with key:@"[key]"
                     NSString *title = [dict objectForKey:@"title"];
                     NSString *img = [dict objectForKey:@"thumbnail"];
                     NSString *data = [dict objectForKey:@"pubDate"];
                     NSString *desc = [dict objectForKey:@"description"];
                     
                     //Create new object
                     
                     News *n = [[News alloc ]initNews:title : img : data : desc];
                     [self addNews:n];
                     [self.tableView reloadData];

                 }
             }
             else
             {
                 NSLog(@"%@",[myError description]);
             }
         }
     }];
}

// Call ShowNewsViewController when tap a New of the table
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ShowNewsViewController *v = [[ShowNewsViewController alloc] initWithNibName:@"ShowNewsViewController" bundle:nil];
    NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
    News *n = [self.news objectAtIndex:selectedIndexPath.row];
    [v setNews:n];
    [[self navigationController] pushViewController:v animated:YES];
}

@end
