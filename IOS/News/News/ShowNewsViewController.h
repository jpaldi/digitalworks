//
//  ShowNewsViewController.h
//  News
//
//  Created by jaldeano on 22/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "News.h"
@interface ShowNewsViewController : UIViewController
- (void) setNews : (News *)n;
@end
