//
//  ShowNewsViewController.m
//  News
//
//  Created by jaldeano on 22/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "ShowNewsViewController.h"
#import "News.h"
@interface ShowNewsViewController (){
    News *news;
}
@property (weak, nonatomic) IBOutlet UITextView *noticiaLabel;
@property (weak, nonatomic) IBOutlet UIImageView *noticiaImg;
@property (weak, nonatomic) IBOutlet UILabel *noticiaDataLabel;
@property (weak, nonatomic) IBOutlet UITextView *noticiaTexto;
- (IBAction)goBackButton:(id)sender;

@end

@implementation ShowNewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Load labels with selected new data
    self.noticiaLabel.text = news.getTitle;
    self.noticiaDataLabel.text = news.getdata;
    self.noticiaTexto.text = news.getDescription;
    // add image to cell view
    NSURL *aURL = [NSURL URLWithString:[[news getLink] stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
    
    UIImage *pic = [UIImage imageWithData:[NSData dataWithContentsOfURL:aURL]];

    self.noticiaImg.image =pic;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setNews : (News *)n{
    news = n;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

// Go back to main page
- (IBAction)goBackButton:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
