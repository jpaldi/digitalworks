//
//  main.m
//  News
//
//  Created by jaldeano on 22/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
