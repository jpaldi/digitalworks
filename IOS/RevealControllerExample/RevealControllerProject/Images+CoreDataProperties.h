//
//  Images+CoreDataProperties.h
//  RevealControllerProject
//
//  Created by jaldeano on 01/09/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Images.h"

NS_ASSUME_NONNULL_BEGIN

@interface Images (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *path;
@property (nullable, nonatomic, retain) NSDecimalNumber *index;
@property (nullable, nonatomic, retain) NSString *name;

@end

NS_ASSUME_NONNULL_END
