//
//  Images+CoreDataProperties.m
//  RevealControllerProject
//
//  Created by jaldeano on 01/09/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Images+CoreDataProperties.h"

@implementation Images (CoreDataProperties)

@dynamic path;
@dynamic index;
@dynamic name;

@end
