//
//  SliderViewController.m
//  RevealControllerProject
//
//  Created by jaldeano on 31/08/16.
//
//

#import "SliderViewController.h"
#import "SWRevealViewController.h"
#import <CommonCrypto/CommonDigest.h>
#import <CoreData/CoreData.h>


@interface SliderViewController (){
    NSMutableArray *images;
    NSMutableArray *texts;
    long imageIndex;
}
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@end
@implementation SliderViewController

@synthesize managedObjectContext = _managedObjectContext;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    imageIndex = -1;
    [self load_fucking_stuff];
    
    //[self load_images_to_array];
    // Do any additional setup after loading the view from its nib.
    SWRevealViewController *revealController = [self revealViewController];
    
    //add left button
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"]
                                                                         style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    //add right button
    UIBarButtonItem *addButtonItem= [[UIBarButtonItem alloc]
                                     initWithImage:[UIImage imageNamed:@"plus.png"]
                                     style:UIBarButtonItemStylePlain target:self
                                     action:@selector(upload_image:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    self.navigationItem.rightBarButtonItem = addButtonItem;
    
    self.imageView.userInteractionEnabled = YES;
    self.revealViewController.panGestureRecognizer.enabled=NO;

    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeft];
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRight];
    
    // Call tapDetected Method when tap in Image
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    [_imageView addGestureRecognizer:singleTap];
    
    [self reloadInputViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tapDetected{
    if (imageIndex > -1){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Index"
                                                        message:[NSString stringWithFormat:@"Image index: %li", imageIndex]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

-(void) swipeRight:(UISwipeGestureRecognizer *) recognizer {
    if(imageIndex == -1){
        return;
    }
    
    if (recognizer.direction == UISwipeGestureRecognizerDirectionRight){
        NSLog(@"swipe right");
        imageIndex++;
        
        // limits of array ("array circular")
        if (imageIndex < 0){
            imageIndex = [images count] - 1 ;
        }
        else if (imageIndex >= [images count]){
            imageIndex = 0 ;
        }
        
        // load image and text of new index
        self.imageView.image = [images objectAtIndex:imageIndex];
        //self.textView.text = [texts objectAtIndex:imageIndex];
    }
}

-(void) swipeLeft:(UISwipeGestureRecognizer *) recognizer {
    
    if(imageIndex == -1){
        return;
    }
    
    if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft){
        NSLog(@"Swipe left");
        
        imageIndex--;
        
        // limits of array ("array circular")
        if (imageIndex < 0){
            imageIndex = [images count] - 1 ;
        }
        else if (imageIndex >= [images count]){
            imageIndex = 0 ;
        }
        
        // load image and text of new index
        self.imageView.image = [images objectAtIndex:imageIndex];
        //self.textView.text = [texts objectAtIndex:imageIndex];
    }
}

// Plus button action
-(void) upload_image:(UIBarButtonItem *) recognizer {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentModalViewController:imagePickerController animated:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    // Dismiss the image selection, hide the picker and
    
    //show the image view with the picked image
    [picker dismissModalViewControllerAnimated:YES];
    
    UIImage *newImage = image;
    // load data to array
    //[images addObject:newImage];
    //[texts addObject:@"[edit text]"];
    
    // Convert Image
    NSData *pngData = UIImagePNGRepresentation(newImage);
    
    //This pulls out PNG data of the image you've captured. From here, you can write it to a file:
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
    
    //Generate name of image by md5 and timestamp
    NSString *imgName;
    imgName = [self generateMD5:[self generateTimeStamp]];
    imgName = [imgName stringByAppendingString:@".png"];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:imgName]; //Add the file name
    [pngData writeToFile:filePath atomically:YES]; //Write the file
    NSLog(@"pathfile guardado: %@", filePath);
    //Store filePath in Data Core
    NSManagedObjectContext *context = [self managedObjectContext];
    
    //NSLog(@"filepath guardado:\n %@\n", filePath);
    
    // Create a new managed object
    NSManagedObject *newStuff = [NSEntityDescription insertNewObjectForEntityForName:@"Images" inManagedObjectContext:context];
    [newStuff setValue:filePath forKey:@"path"];
    [newStuff setValue:imgName forKey:@"name"];
    
    imageIndex++;

    [newStuff setValue:[NSNumber numberWithInt:imageIndex] forKey:@"index"];
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }

    [self load_fucking_stuff];
}

- (NSString *) generateMD5:(NSString *) input
{
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, strlen(cStr), digest );
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
}

- (NSString *) generateTimeStamp {
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"%H:%M:%S"];
    return [formatter stringFromDate:date];
}

- (void) load_fucking_stuff{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Images"];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"index" ascending:YES];
    [request setSortDescriptors:@[sort]];
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    
    NSMutableArray *fullPaths = [results valueForKey:@"path"];
    images = [NSMutableArray new];
    texts = [NSMutableArray new];
    int i;
    for (i = 0; i < fullPaths.count ; i++){
        //NSLog(@"filepath carregado:\n %@\n", [fullPaths objectAtIndex:i]);
        
        // get current file path
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
        NSString *imgName = [fullPaths objectAtIndex:i];
        
        NSArray *testArray2 = [imgName componentsSeparatedByString:@"/"];
        int k;
        for (k=0; k < testArray2.count ; k++){
            if ([[testArray2 objectAtIndex:k] containsString:@".png"]){
                NSString *filePath = [documentsPath stringByAppendingPathComponent:[testArray2 objectAtIndex:k]]; //Add the file name
                UIImage *x = [UIImage imageWithContentsOfFile:filePath];
                [images addObject:x];
            }
        }
        k = 0;
    }
    i=0;
    if (images.count > 0){
        [self update_view_elements];
    }
}

- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void) update_view_elements{
    self.imageView.image = [images objectAtIndex:0];
    imageIndex  = 0;
}
@end
