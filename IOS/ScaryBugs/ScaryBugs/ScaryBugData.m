//
//  ScaryBugData.m
//  ScaryBugs
//
//  Created by jaldeano on 08/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "ScaryBugData.h"

@implementation ScaryBugData

@synthesize title = _title;
@synthesize rating = _rating;

- (id)initWithTitle:(NSString*)title rating:(float)rating {
    if ((self = [super init])) {
        self.title = title;
        self.rating = rating;
    }
    return self;
}

@end