//
//  RootViewController.m
//  Slide View
//
//  Created by jaldeano on 23/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "RootViewController.h"

@interface RootViewController (){
    NSArray *images;
    NSArray *texts;
    long imageIndex;
}
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextView *textViewSporting;
- (IBAction)swipeGestureAction:(id)sender;
@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.imageView.userInteractionEnabled = YES;
    
    // Data to show
    images = [[NSArray alloc] initWithObjects:@"images.png",@"linux.png",@"sporting.png", @"mjv.png",nil];
    
    texts = [[NSArray alloc] initWithObjects:@"O estádio mais lindo do mundo.",@"Até o equipamento secundário é lindo",@"Para não falar deste maravilhoso símbolo, QUE COISA LINDA! Viva o SPORTING!", @"Até a Maria José Valério marchava!", nil];
    
    imageIndex = 0;
    self.textViewSporting.text = [texts objectAtIndex:imageIndex];
    
    // Call tapDetected Method when tap in Image
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    [_imageView addGestureRecognizer:singleTap];
    [self reloadInputViews];
}

// Show allert with the index of image taped
-(void)tapDetected{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Index"
                                                    message:[NSString stringWithFormat:@"Image index: %li", imageIndex]
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Image swipe implementation
- (IBAction)swipeGestureAction:(id)sender {
    
    UISwipeGestureRecognizerDirection direction = [(UISwipeGestureRecognizer *) sender direction];
    
    switch (direction){
        // swipe left
        case UISwipeGestureRecognizerDirectionLeft:
            NSLog(@"swipe left");
            imageIndex++;
            break;
        //swipe right
        case UISwipeGestureRecognizerDirectionRight:
            NSLog(@"swipe right");
            imageIndex--;
            break;
        default:
            break;
    }
    
    // limits of array ("array circular")
    if (imageIndex < 0){
        imageIndex = [images count] - 1 ;
    }
    else if (imageIndex > [images count]){
        imageIndex = 0 ;
    }
    
    // load image and text of new index
    self.imageView.image = [UIImage imageNamed:[images objectAtIndex:imageIndex]];
    self.textViewSporting.text = [texts objectAtIndex:imageIndex];
}
@end
