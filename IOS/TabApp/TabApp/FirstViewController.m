//
//  FirstViewController.m
//  TabApp
//
//  Created by jaldeano on 22/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()
- (IBAction)searchButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong) NSMutableArray *words;
- (IBAction)refreshButtonAction:(id)sender;
@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.words = [NSMutableArray new];
    [self.words addObject:@"ola"];
    [self.words addObject:@"ola1"];
    [self.words addObject:@"digital"];
    [self.words addObject:@"works"];
    [self.words addObject:@"joao"];
    self.searchTextField.text = @"";
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// returns the number of highscores
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.words count];
}


// Loads the Array Data to TableView
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    NSString *ns = [self.words objectAtIndex:indexPath.row];
    cell.textLabel.text = ns;
    [cell setUserInteractionEnabled:NO];

    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

//Search for textfield word on table and if find change cell background to yellow
- (IBAction)searchButtonAction:(id)sender {
    [self refreshButtonAction:sender];
    if (self.searchTextField.text != nil){

        for (int i = 0; i < [self.words count] ; i++){
            if ([[self.words objectAtIndex:i] isEqualToString:self.searchTextField.text]){                NSIndexPath *indexPath=[NSIndexPath indexPathForRow:i inSection:0];
                UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
                [cell setBackgroundColor:[UIColor yellowColor]];
                
                break;
            }
            
        }
    }
}

// Table cells turn to white
- (IBAction)refreshButtonAction:(id)sender {
    for (int i = 0; i < [self.words count] ; i++){
            NSIndexPath *indexPath=[NSIndexPath indexPathForRow:i inSection:0];
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            [cell setBackgroundColor:[UIColor whiteColor]];
    }

}

//Closes text keyboard when background is clicked
- (IBAction)backgroundClick:(id)sender {
    [self.searchTextField resignFirstResponder];
}

@end
