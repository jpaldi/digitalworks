//
//  GameOverViewController.h
//  Tap Me with XIBS
//
//  Created by jaldeano on 17/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
@interface GameOverViewController : UIViewController{
    NSInteger score;
    Singleton *singleton;
}
- (IBAction)submitScoreButton:(id)sender;
- (IBAction)backgroundClick:(id)sender;
- (IBAction)doneButton:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
- (IBAction)ignore:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
- (void) setScore:(NSInteger)scr;
@end
