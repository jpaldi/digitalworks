//
//  GameOverViewController.m
//  Tap Me with XIBS
//
//  Created by jaldeano on 17/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "GameOverViewController.h"
#import "Player.h"
#import "CoreData/CoreData.h"

@interface GameOverViewController ()
@end

@implementation GameOverViewController

- (void) setScore:(NSInteger)scr{
    NSLog(@"Score:%li",(long)scr);
    score = scr;
}

- (void)viewDidLoad {
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"bg_tile.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.scoreLabel.text = [NSString stringWithFormat:@"Score:%li",(long)score];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (IBAction)submitScoreButton:(id)sender {
    Player *x = [[Player alloc] initPlayer:self.usernameTextField.text :score];
    [singleton addHighscore:x];
    
    // INSERT CORE DATA
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Create a new managed object
    NSManagedObject *newDevice = [NSEntityDescription insertNewObjectForEntityForName:@"Highscore" inManagedObjectContext:context];
    [newDevice setValue:self.usernameTextField.text forKey:@"username"];
    NSString *saveScore = [NSString stringWithFormat:@"%ld", score];
    [newDevice setValue:saveScore forKey:@"score"];
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    
    [self.navigationController popToRootViewControllerAnimated:YES];

}

- (IBAction)backgroundClick:(id)sender {
    [self.usernameTextField resignFirstResponder];
}

- (IBAction)doneButton:(id)sender {
    [self.usernameTextField resignFirstResponder];
}
- (IBAction)ignore:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];

}
@end
