//
//  HighscoresViewController.h
//  Tap Me with XIBS
//
//  Created by jaldeano on 18/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
@interface HighscoresViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
    // Used when data saving was not persistent
    Singleton *singleton;
}
- (IBAction)goBackButton:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong) NSMutableArray *highscores;

@end
