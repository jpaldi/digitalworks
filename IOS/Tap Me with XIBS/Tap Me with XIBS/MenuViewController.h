//
//  MenuViewController.h
//  Tap Me with XIBS
//
//  Created by jaldeano on 17/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//
#import "Player.h"
#import <UIKit/UIKit.h>
#import "Singleton.h"
@interface MenuViewController : UIViewController
{
    Singleton *singleton;
}

- (IBAction)playButton:(id)sender;
- (IBAction)highscoresButton:(id)sender;
- (IBAction)exitButton:(id)sender;
@end
