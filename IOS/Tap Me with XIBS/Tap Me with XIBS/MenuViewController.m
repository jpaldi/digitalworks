//
//  MenuViewController.m
//  Tap Me with XIBS
//
//  Created by jaldeano on 17/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//
#import "Singleton.h"
#import "MenuViewController.h"
#import "PlayViewController.h"
#import "HighscoresViewController.h"
#import "Player.h"
@interface MenuViewController ()
@end

@implementation MenuViewController

- (void)viewDidLoad {
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"bg_tile.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];

    singleton = [Singleton sharedInstance];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)playButton:(id)sender {
    PlayViewController *pv = [[PlayViewController alloc] initWithNibName:@"PlayViewController" bundle:nil];
    [[self navigationController] pushViewController:pv animated:YES];
}

- (IBAction)highscoresButton:(id)sender {
    NSLog(@"HIGHSCORES: \n");
    NSLog(@"%@", [singleton toString]);
    HighscoresViewController *hs = [[HighscoresViewController alloc] initWithNibName:@"HighscoresViewController" bundle:nil];
    [[self navigationController] pushViewController:hs animated:YES];
}

- (IBAction)exitButton:(id)sender {
    exit(0);
}
@end
