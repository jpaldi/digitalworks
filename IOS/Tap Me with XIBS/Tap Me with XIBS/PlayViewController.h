//
//  PlayViewController.h
//  Tap Me with XIBS
//
//  Created by jaldeano on 17/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayViewController : UIViewController
{
    NSInteger count;
    NSInteger seconds;
    NSTimer *timer;
}

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;

- (IBAction)tapButtonAction:(id)sender;
@end
