//
//  PlayViewController.m
//  Tap Me with XIBS
//
//  Created by jaldeano on 17/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "PlayViewController.h"
#import "GameOverViewController.h"

@interface PlayViewController ()
@end

@implementation PlayViewController

- (void)viewDidLoad {
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"bg_tile.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupGame];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)tapButtonAction:(id)sender {
    count++;
    NSLog(@"%ld", (long)count);
    [self.scoreLabel setText:[NSString stringWithFormat:@"Score %ld", (long)count]];
}

- (void)setupGame {
    // 1
    seconds = 5;
    count = 0;
    
    // 2
    self.timeLabel.text = [NSString stringWithFormat:@"Time: %ld", (long)seconds];
    self.scoreLabel.text = [NSString stringWithFormat:@"Score\n%ld", (long)count];
    
    // 3
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                             target:self
                                           selector:@selector(subtractTime)
                                           userInfo:nil
                                        repeats:YES];
}

- (void)subtractTime {
    // 1
    seconds--;
    self.timeLabel.text = [NSString stringWithFormat:@"Time: %ld",(long)seconds];
    
    // 2
    if (seconds == 0) {
        [timer invalidate];
        
        GameOverViewController *go = [[GameOverViewController alloc] initWithNibName:@"GameOverViewController" bundle:nil];
        [go setScore:count];
        [[self navigationController] pushViewController:go animated:YES];
        
    }
}
@end
