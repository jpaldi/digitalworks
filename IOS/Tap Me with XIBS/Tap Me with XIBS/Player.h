//
//  Player.h
//  Tap Me with XIBS
//
//  Created by jaldeano on 17/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Player : NSObject
@property NSString *username;
@property NSInteger score;
- (Player *) initPlayer: (NSString*)n :(NSInteger) s;
- (void) setHighscore : (NSInteger) n;
@end
