//
//  Player.m
//  Tap Me with XIBS
//
//  Created by jaldeano on 17/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "Player.h"

@implementation Player
- (Player *) initPlayer: (NSString*)n :(NSInteger) s{
    self = [super init];
    if (self) {
        self.username = n;
        self.score = s;
    }
    return self;
}

- (NSString *) toString {
    NSString * ret = [[NSString alloc] initWithFormat:@""];
    
    ret = [NSString stringWithFormat:@"%@     |     %li", self.username, (long)self.score];
    
    return ret;
}

- (void) setHighscore : (NSInteger) n{
    self.score = n;
}

@end
