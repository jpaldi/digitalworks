//
//  Singleton.h
//  Tap Me with XIBS
//
//  Created by jaldeano on 18/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Player.h"
@interface Singleton : NSObject
@property (nonatomic, retain) NSMutableArray *highscores;
- (NSString *) toString;
+ (Singleton*)sharedInstance;
- (void) addHighscore: (Player *) p;
- (void) sortArray;
@end
