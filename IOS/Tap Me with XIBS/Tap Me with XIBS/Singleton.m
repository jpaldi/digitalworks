//
//  Singleton.m
//  Tap Me with XIBS
//
//  Created by jaldeano on 18/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "Singleton.h"
#import "Player.h"
@implementation Singleton
#pragma mark - singleton method

@synthesize highscores;


+ (Singleton*)sharedInstance
{
    static dispatch_once_t predicate = 0;
    __strong static id sharedObject = nil;
    dispatch_once(&predicate, ^{
        sharedObject = [[self alloc] init];
    });
    return sharedObject;
}

- (id)init {
    if (self = [super init]) {
        highscores = [[NSMutableArray alloc] initWithCapacity:10];
    }
    return self;
}

// add highscore to singleton array: highscores
- (void) addHighscore: (Player *) p{
    for (int i = 0; i < highscores.count ; i++){
        if ([[[highscores objectAtIndex:i] username] isEqualToString:p.username])
        {
            if ([[highscores objectAtIndex:i] score] < p.score){
                [[highscores objectAtIndex:i] setScore:p.score];
            }
            return;
        }
    }
    [highscores addObject:p];
}

//returns a string with usernames and scores
- (NSString *) toString {
    NSString * ret = [[NSString alloc] initWithFormat:@""];
    
    for (int i = 0 ; i < highscores.count ; i++){
        ret = [NSString stringWithFormat:@"%@ \n %i) %@ %li", ret , i+1, [[highscores objectAtIndex:i] username], (long)[[highscores objectAtIndex:i] score]];
    }
    
    return ret;
}

- (void) sortArray{
    [highscores sortUsingDescriptors:
     @[
       [NSSortDescriptor sortDescriptorWithKey:@"score" ascending:NO],
       ]];
}
@end
