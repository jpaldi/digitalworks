//
//  main.m
//  Tap Me with XIBS
//
//  Created by jaldeano on 17/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
