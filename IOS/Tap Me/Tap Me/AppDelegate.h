//
//  AppDelegate.h
//  Tap Me
//
//  Created by jaldeano on 11/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

