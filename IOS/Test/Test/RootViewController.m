//
//  RootViewController.m
//  Test
//
//  Created by jaldeano on 25/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "RootViewController.h"

@interface RootViewController (){
    NSArray *images;
    NSArray *texts;
    long imageIndex;
}
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.imageView.userInteractionEnabled = YES;
    
    // Data to show
    images = [[NSArray alloc] initWithObjects:@"exit.png",@"facebook.png",@"video.png", @"cal.png",nil];
    
    texts = [[NSArray alloc] initWithObjects:@"exit butao bueda fixe.",@"facebook butao bue da cenas",@"Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.", @"viva o sporting", nil];
    
    imageIndex = 0;
    self.textView.text = [texts objectAtIndex:imageIndex];
    self.imageView.image = [UIImage imageNamed:[images objectAtIndex:imageIndex]];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeft];
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRight];
    
    // Call tapDetected Method when tap in Image
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    [_imageView addGestureRecognizer:singleTap];
    [self reloadInputViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Show allert with the index of image taped
-(void)tapDetected{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Index"
                                                    message:[NSString stringWithFormat:@"Image index: %li", imageIndex]
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

-(void) swipeRight:(UISwipeGestureRecognizer *) recognizer {
    if (recognizer.direction == UISwipeGestureRecognizerDirectionRight){
        NSLog(@"swipe right");
        imageIndex++;
        
        // limits of array ("array circular")
        if (imageIndex < 0){
            imageIndex = [images count] - 1 ;
        }
        else if (imageIndex >= [images count]){
            imageIndex = 0 ;
        }
        
        // load image and text of new index
        self.imageView.image = [UIImage imageNamed:[images objectAtIndex:imageIndex]];
        self.textView.text = [texts objectAtIndex:imageIndex];
    }
}

-(void) swipeLeft:(UISwipeGestureRecognizer *) recognizer {
    if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft){
        NSLog(@"Swipe left");
        
        imageIndex--;
        
        // limits of array ("array circular")
        if (imageIndex < 0){
            imageIndex = [images count] - 1 ;
        }
        else if (imageIndex >= [images count]){
            imageIndex = 0 ;
        }
        
        // load image and text of new index
        self.imageView.image = [UIImage imageNamed:[images objectAtIndex:imageIndex]];
        self.textView.text = [texts objectAtIndex:imageIndex];
    }
}

@end
