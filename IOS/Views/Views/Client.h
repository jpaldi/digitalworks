//
//  Client.h
//  Views
//
//  Created by jaldeano on 16/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Client : NSObject

@property (strong) NSString * username;
@property (strong) NSString * password;
@end
