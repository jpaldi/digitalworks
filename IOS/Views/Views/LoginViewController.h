//
//  LoginViewController.h
//  Views
//
//  Created by jaldeano on 16/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Client.h"
@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *usernameNameTextField;
@property Client *username;
- (void) setClient : (Client *) cl;
@end
