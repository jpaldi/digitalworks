//
//  LoginViewController.m
//  Views
//
//  Created by jaldeano on 16/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "LoginViewController.h"
#import "RootViewController.h"
#import "Client.h"

@interface LoginViewController ()
- (IBAction)logoutButton:(id)sender;
@end

@implementation LoginViewController

- (void)viewDidLoad {
    Client *cl = self.username;
    self.usernameNameTextField.text = [NSString stringWithFormat:@"Welcome, %@!", cl.username];

    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)logoutButton:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void) setClient : (Client *) cl {
    self.username = cl;
}
@end
