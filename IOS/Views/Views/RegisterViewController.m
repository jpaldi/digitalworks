//
//  RegisterViewController.m
//  Views
//
//  Created by jaldeano on 17/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "RegisterViewController.h"
#import "Client.h"
#import "RootViewController.h"

@interface RegisterViewController ()
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordConfirmTextField;
- (IBAction)submitButton:(id)sender;
- (IBAction)goBackButton:(id)sender;
@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)goBackButton:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];

}
- (IBAction)submitButton:(id)sender {
    for (int i=0; i<self.clients_array.count; i++){
        if ([[(Client *)[self.clients_array objectAtIndex:i] username] isEqualToString:self.usernameTextField.text]){
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Choose other username."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            return;
        }
    }
    
    if (![self.passwordConfirmTextField.text isEqualToString:self.passwordTextField.text])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Passwords not match."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else{
        Client * cl = [[Client alloc] init];
        cl.password = self.passwordTextField.text;
        cl.username = self.usernameTextField.text;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Create"
                                                        message:@"It was created a new client."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        RootViewController *rootController =
        (RootViewController *)
        [self.navigationController.viewControllers objectAtIndex: 0];
        
        [rootController.clients_array addObject:cl];
        
        [self.navigationController popToRootViewControllerAnimated:YES];


    }
}
@end
