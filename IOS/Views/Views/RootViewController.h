//
//  RootViewController.h
//  Views
//
//  Created by jaldeano on 16/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UIViewController
@property (strong) NSMutableArray *clients_array;
@end
