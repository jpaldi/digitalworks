//
//  RootViewController.m
//  Views
//
//  Created by jaldeano on 16/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "RootViewController.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "Client.h"


@interface RootViewController ()
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
- (IBAction)submitAction:(id)sender;
- (IBAction)goToRegister:(id)sender;
@end

@implementation RootViewController

- (void)viewDidLoad {
    self.clients_array = [[NSMutableArray alloc] initWithCapacity:10];
    Client * admin = [Client alloc];
    admin.username = @"admin";
    admin.password = @"a";
    [self.clients_array addObject:admin];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)clickBackground:(id)sender {
    [self.passwordTextField resignFirstResponder];
    [self.nameTextField resignFirstResponder];
}

- (IBAction)doneNameKeyboard:(id)sender {
    [self resignFirstResponder];
}
- (IBAction)donePasswordKeyboard:(id)sender {
    [self resignFirstResponder];
}


- (IBAction)submitAction:(id)sender {
    for (int i=0 ; i<self.clients_array.count ; i++){
        if ([[(Client *)[self.clients_array objectAtIndex:i] username] isEqualToString:self.nameTextField.text] && [[(Client *)[self.clients_array objectAtIndex:i] password] isEqualToString:self.passwordTextField.text]){
            NSLog(@"log in sucessfull");
            LoginViewController *vc2 = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
            vc2.usernameNameTextField.text = [NSString stringWithFormat:@""];
            [vc2 setClient:[self.clients_array objectAtIndex:i]];
            self.nameTextField.text = @"";
            self.passwordTextField.text = @"";
            [[self navigationController] pushViewController:vc2 animated:YES];
            return;
        }
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:@"Wrong username or password."
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (IBAction)goToRegister:(id)sender{
    RegisterViewController *v = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    v.clients_array = [self.clients_array copy];
    [[self navigationController] pushViewController:v animated:YES];
}
@end
