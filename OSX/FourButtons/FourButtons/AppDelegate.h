//
//  AppDelegate.h
//  FourButtons
//
//  Created by jaldeano on 02/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "RootViewController.h"

@interface AppDelegate : NSObject <NSApplicationDelegate>
@property (weak) IBOutlet NSWindow *window;
@property (nonatomic,strong) IBOutlet RootViewController *rootViewController;
@end

