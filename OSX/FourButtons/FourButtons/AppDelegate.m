//
//  AppDelegate.m
//  FourButtons
//
//  Created by jaldeano on 02/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "AppDelegate.h"
#import "Client.h"
#import "Provider.h"
#import "Article.h"
#import "RootViewController.h"
@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    // 1. Create the master View Controller
    self.rootViewController = [[RootViewController alloc] initWithNibName:@"RootViewController" bundle:nil];
    
    Client *jp;
    jp = [[Client alloc] initWithNameAgeAndCountry:@"João Aldeano" :23 :@"Portugal"];
    
    Provider *dg;
    dg = [[Provider alloc] initWithNameAndCountry:@"DigitalWorks": @"Portugal"];
    
    Article *mj;
    mj = [[Article alloc] initWithIdNameAndPrice:@"Haxixe" :1 :10.10];

    [self.rootViewController initializeClientList];
    [self.rootViewController addClientToList:jp];
    
    [self.rootViewController initializeArticleList];
    [self.rootViewController addArticleToList:mj];
    
    [self.rootViewController initializeProviderList];
    [self.rootViewController addProviderToList:dg];
    
    // 2. Add the view controller to the Window's content view
    [self.window.contentView addSubview:self.rootViewController.view];
    self.rootViewController.view.frame = ((NSView*)self.window.contentView).bounds;
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
