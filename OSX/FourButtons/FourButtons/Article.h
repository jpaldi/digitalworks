//
//  Article.h
//  FourButtons
//
//  Created by jaldeano on 08/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Article : NSObject
@property (nonatomic) NSInteger id;
@property (nonatomic) NSString * name;
@property (nonatomic) float price;
- (Article *) initWithIdNameAndPrice: (NSString *)name : (NSInteger)id :
(float)price;
@end
