//
//  Article.m
//  FourButtons
//
//  Created by jaldeano on 08/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "Article.h"

@implementation Article
- (Article *) initWithIdNameAndPrice: (NSString *)name : (NSInteger)id :
(float)price{
    self = [super init];
    if (self) {
        self.name = name;
        self.id = id;
        self.price = price;
    }
    return self;
}
@end
