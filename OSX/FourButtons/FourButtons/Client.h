//
//  Client.h
//  Tutorial2
//
//  Created by jaldeano on 29/07/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

@interface Client : Person
@property (nonatomic) double balance;
@property (nonatomic) NSInteger age;

- (Client *)initWithNameAgeAndCountry: (NSString *)name : (NSInteger)age : (NSString *)country;
- (void) addClientCredit : (double) f ;
- (void) initBalance;
- (NSString * ) toString;
@end
