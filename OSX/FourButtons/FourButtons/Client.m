//
//  Client.m
//  Tutorial2
//
//  Created by jaldeano on 29/07/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "Client.h"

@implementation Client


- (Client *) initWithNameAgeAndCountry: (NSString *)name : (NSInteger)age :
(NSString *)country{
    
    self = [super init];
    if (self) {
        self.name = name;
        self.age = age;
        self.country = country;
        self.balance = 0.0;
    }
    return self;
    
}

- (void) initBalance{
    self.balance = 0;
}

- (void) addClientCredit: (double) b{
    self.balance += b;
}


- (NSString *) toString{
    return [NSString stringWithFormat:@"%@ | %@| %f", self.name, self.country, self.balance];

}
@end
