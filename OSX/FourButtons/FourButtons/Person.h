//
//  Person.h
//  FourButtons
//
//  Created by jaldeano on 08/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject
@property (nonatomic) NSString * name;
@property (nonatomic) NSString * country;
@end
