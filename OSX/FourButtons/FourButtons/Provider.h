//
//  Provider.h
//  FourButtons
//
//  Created by jaldeano on 08/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"
@interface Provider : Person
- (Provider *) initWithNameAndCountry: (NSString *)name :
(NSString *)country;
@end
