//
//  Provider.m
//  FourButtons
//
//  Created by jaldeano on 08/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "Provider.h"

@implementation Provider
- (Provider *) initWithNameAndCountry: (NSString *)name :
(NSString *)country{
    
    self = [super init];
    if (self) {
        self.name = name;
        self.country = country;
    }
    return self;
    
}
@end
