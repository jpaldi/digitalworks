//
//  RootViewController.h
//  FourButtons
//
//  Created by jaldeano on 02/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Client.h"
#import "Provider.h"
#import "Article.h"
#import "RootViewController.h"

#pragma client view
@interface RootViewController : NSViewController <NSTableViewDelegate>

@property NSInteger segment_ind;
@property (weak) IBOutlet NSView *clientMenuView;

@property (strong) NSMutableArray *clients;
@property (weak) IBOutlet NSButton *quitButton;

//create new and edit client view
@property (weak) IBOutlet NSView *createClientView;
@property (weak) IBOutlet NSView *balanceView;

@property (weak) IBOutlet NSTextField *clientNameTextField;
@property (weak) IBOutlet NSTextField *clientAgeTextField;
@property (weak) IBOutlet NSTextField *insertCreditTextField;
@property (weak) IBOutlet NSSegmentedCell *countryChoiceOption;
@property (weak) IBOutlet NSTextField *clientcountryTextField;
@property (weak) IBOutlet NSButton *saveButton;
@property (weak) IBOutlet NSButton *finishEditingButton;
@property (weak) IBOutlet NSTextField *balanceTextView;


//left buttons view
@property (weak) IBOutlet NSView *buttonsView;

@property (weak) IBOutlet NSButton *createClientButton;

//client table view
@property (weak) IBOutlet NSScrollView *clientsTableView;
@property (weak) IBOutlet NSTableView *clientsTable;

//age error view
@property (strong) IBOutlet NSView *errorAgeView;


// ACTIONS
- (IBAction)cancelButton:(id)sender;
- (IBAction)saveButton:(id)sender;
- (IBAction)newClientButton:(id)sender;
- (IBAction)removeClientButton:(id)sender;
- (IBAction)insertCreditTextField:(id)sender;
- (IBAction)editClientsAction:(id)sender;
- (IBAction)finishEditingAction:(id)sender;\
- (IBAction)goBackAction:(id)sender;
- (IBAction)onSegmentValueChanged:(id)sender;

//client class methods
- (void) initializeClientList;
- (void) addClientToList:(Client*)cl;

#pragma main menu view
@property (strong) IBOutlet NSView *mainMenuView;
- (IBAction)clientMenuAction:(id)sender;
- (IBAction)providerMenuAction:(id)sender;
- (IBAction)articleMenuAction:(id)sender;


#pragma provider menu view
@property (strong) NSMutableArray *providers;
@property (weak) IBOutlet NSTableView *providersTable;

@property (weak) IBOutlet NSView *providerMenuView;
@property (weak) IBOutlet NSView *providerButtonView;
- (IBAction)newProviderButton:(id)sender;
- (IBAction)removeProviderButton:(id)sender;
- (IBAction)editProviderButton:(id)sender;
- (IBAction)goBackProviderButton:(id)sender;

- (void) initializeProviderList;
- (void) addProviderToList:(Provider*)pr;

// Create new provider window
@property (strong) IBOutlet NSView *createNewProviderView;
@property (weak) IBOutlet NSTextField *providerNameTextFieald;
@property (weak) IBOutlet NSSegmentedControl *providerCountryChoice;
@property (weak) IBOutlet NSTextField *providerCountryTextField;
@property (weak) IBOutlet NSScrollView *providerTableScrollView;
- (IBAction)providerNewSaveButton:(id)sender;
- (IBAction)providerNewCancelButton:(id)sender;
- (IBAction)onProviderCountryChoiceSegmentChange:(id)sender;

@property (weak) IBOutlet NSButton *providerFinishEditing;
@property (weak) IBOutlet NSButton *saveNewProviderButton;

#pragma Article menu view
@property (strong) NSMutableArray *articles;
@property (weak) IBOutlet NSView *articleMenuView;

//article table
@property (weak) IBOutlet NSScrollView *articleViewTable;
@property (weak) IBOutlet NSTableView *articleTable;

//article button view
@property (weak) IBOutlet NSView *articleButtonView;

- (IBAction)newArticleButtonAction:(id)sender;
- (IBAction)removeArticleButtonAction:(id)sender;
- (IBAction)editArticleButtonAction:(id)sender;
- (IBAction)goBackArticleButton:(id)sender;

//create article view

@property (weak) IBOutlet NSView *createArticleView;

@property (weak) IBOutlet NSTextField *articleNameTextField;

@property (weak) IBOutlet NSTextField *articleIDTextField;

@property (weak) IBOutlet NSTextField *articlePriceTextView;

@property (weak) IBOutlet NSButton *finishEditingArticle;
@property (weak) IBOutlet NSButton *createArticleButton;
- (IBAction)newArticleButton:(id)sender;
- (IBAction)removeArticleButton:(id)sender;
- (IBAction)editArticleButton:(id)sender;


- (IBAction)createArticleButtonAction:(id)sender;
- (IBAction)articleFinishEditButtonAction:(id)sender;
- (IBAction)cancelArticleCreateButtonAction:(id)sender;

- (void) initializeArticleList;
- (void) addArticleToList:(Article*)at;
- (IBAction)wrongArticleGoBack:(id)sender;
-(Article*) selectedArticle;

@property (weak) IBOutlet NSButton *wrongClientGoBackButton;
@property (weak) IBOutlet NSButton *wrongArticleGoBackButton;

@end
