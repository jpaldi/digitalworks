//
//  RootViewController.m
//  FourButtons
//
//  Created by jaldeano on 02/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import "RootViewController.h"
#import "Client.h"
#import "Provider.h"
#include <math.h>

@interface RootViewController ()
@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
    [self.createClientView setHidden:NO];
    [self.errorAgeView setHidden:YES];
    [self.balanceView setHidden:YES];
    [self.buttonsView setHidden:YES];
    [self.clientsTableView setHidden:YES];
    [self.providerMenuView setHidden:YES];
    [self.createClientView setHidden: YES];
    [self.articleMenuView setHidden:YES];
    
}

- (IBAction)quitButton:(id)sender {
    [self.clientMenuView setHidden:YES];
    [self.mainMenuView setHidden:NO];
    [self.buttonsView setHidden:YES];
    [self.clientsTableView setHidden:YES];
}

- (void) initializeClientList{
    self.clients = [[NSMutableArray alloc] init];
}

- (void) addClientToList:(Client*)cl{
    [self.clients addObject:cl];
}

- (IBAction)newClientButton:(id)sender {
    [self.clientsTableView setHidden:YES];
    [self.createClientView setHidden:NO];
    [self.buttonsView setHidden:YES];
    [self.finishEditingButton setHidden:YES];
    [self.saveButton setHidden:NO];
    [self.balanceView setHidden:YES];

}

- (IBAction)removeClientButton:(id)sender {
    // 1. Get selected doc
    Client *client = [self selectedClient];
    if (client)
    {
        [self.clients removeObject:client];
        // 3. Remove the selected row from the table view.
        [self.clientsTable removeRowsAtIndexes:[NSIndexSet indexSetWithIndex:self.clientsTable.selectedRow] withAnimation:NSTableViewAnimationSlideRight];

    }
}

- (IBAction)insertCreditTextField:(id)sender {
    Client *client = [self selectedClient];
    if( client )
    {   float val = [self.insertCreditTextField floatValue];
        CGFloat rounded = floorf(val * 100 + 0.5) / 100;
        [client addClientCredit:rounded];
        NSIndexSet * indexSet = [NSIndexSet indexSetWithIndex:[self.clients indexOfObject:client]];
        NSIndexSet * columnSet = [NSIndexSet indexSetWithIndex:0];
        [self.clientsTable reloadDataForRowIndexes:indexSet columnIndexes:columnSet];
        self.insertCreditTextField.stringValue = @"";
        [self.clientsTable needsLayout];
        [self.clientsTable reloadData];
    }
}


- (IBAction)cancelButton:(id)sender {
    [self.countryChoiceOption setSelectedSegment:self.segment_ind];
    [self.clientsTableView setHidden:NO];
    [self.createClientView setHidden:YES];
    [self.buttonsView setHidden:NO];
    self.clientNameTextField.stringValue = @"";
    self.clientAgeTextField.stringValue = @"";
    self.balanceTextView.stringValue = @"";
    self.clientcountryTextField.stringValue = @"";

}

- (IBAction)saveButton:(id)sender {
    NSString * name = [self.clientNameTextField stringValue];
    NSString * age = self.clientAgeTextField.stringValue;
    Client *cl;
    if ([self isDecimal:age]){
        if ([self countryChoiceOption].selectedSegment == 0) {
            cl = [[Client alloc] initWithNameAgeAndCountry:name :age.integerValue :@"Portugal"];
        }
        else if([self countryChoiceOption].selectedSegment == 1) {
            cl = [[Client alloc] initWithNameAgeAndCountry:name :age.integerValue :@"UK"];
        }
        else if([self countryChoiceOption].selectedSegment == 2) {
            cl = [[Client alloc] initWithNameAgeAndCountry:name :age.integerValue :@"USA"];
        }
        else if([self countryChoiceOption].selectedSegment == 3) {
            NSString* country = self.clientcountryTextField.stringValue;
             cl = [[Client alloc] initWithNameAgeAndCountry:name :age.integerValue :country];

        }
        else{
            
        }
        
        [self addClientToList:cl];
        
        
        [self.clientsTable setHidden:NO];
        [self.createClientView setHidden:YES];
        [self.clientsTableView setHidden:NO];
        [self.buttonsView setHidden:NO];
        self.clientNameTextField.stringValue = @"";
        self.clientAgeTextField.stringValue = @"";
        self.clientcountryTextField.stringValue = @"";

        [self.clientsTable needsLayout];
        [self.clientsTable reloadData];
    }
    else{
        [self.errorAgeView setHidden:NO];
        [self.clientMenuView setHidden:YES];
        [self.createClientView setHidden:YES];
        [self.clientsTable needsLayout];
        [self.clientsTable reloadData];
    }
}


- (BOOL) isDecimal: (NSString*) str{
    NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
    BOOL isDecimal = [nf numberFromString:str] != nil;
    
    if (isDecimal){
        return YES;
    }
    else{
        return NO;
    }
    
}

- (NSInteger) verify_country: (NSString*) s{
    if ([s isEqualToString:@"Portugal"]){
        return 0;
    }
    else if ([s isEqualToString:@"UK"]){
        return 1;
    }
    else if ([s isEqualToString:@"USA"]){
        return 2;
    }
    else{
        return -1;
    }
}

- (void)awakeFromNib {
    [self.clientsTable setTarget:self];
    [self.clientsTable setDoubleAction:@selector(doubleClick:)];
    
    [self.providersTable setTarget:self];
    [self.providersTable setDoubleAction:@selector(doubleClick:)];
    
    [self.articleTable setTarget:self];
    [self.articleTable setDoubleAction:@selector(doubleClick:)];
}

- (void)doubleClick:(id)object {
    // This gets called after following steps 1-3.
    //NSInteger rowNumber = [self.clientsTable clickedRow];
    // Do something...
    Client *cl = [self selectedClient];
    if (cl)
    {
        self.clientNameTextField.stringValue = cl.name;
        self.clientAgeTextField.integerValue = cl.age;
        self.balanceTextView.stringValue = [NSString stringWithFormat:@"%0.2f", cl.balance];
        
        NSString * country = cl.country;
        NSInteger seg = [self verify_country:country];
        
        if(seg == -1){
            [self.countryChoiceOption setSelectedSegment:3];
            [self.clientcountryTextField setHidden:NO];
        }
        else{
            [self.countryChoiceOption setSelectedSegment:seg];
            [self.clientcountryTextField setHidden:YES];
        }
        [self.clientsTableView setHidden:YES];
        [self.balanceView setHidden:NO];
        [self.createClientView setHidden:NO];
        [self.finishEditingButton setHidden:NO];
        [self.buttonsView setHidden:YES];
        [self.saveButton setHidden:YES];
        [self.wrongArticleGoBackButton setHidden:YES];
        return;
    }
    // PROVIDERS SELECTION
    
    Provider *pr = [self selectedProvider];
    if (pr){
        self.providerNameTextFieald.stringValue = pr.name;
        self.providerCountryTextField.stringValue = pr.country;
        NSString * country = pr.country;
        NSInteger seg = [self verify_country:country];
        
        if(seg == -1){
            [self.providerCountryChoice setSelectedSegment:3];
            [self.providerCountryTextField setHidden:NO];
        }
        else{
            [self.providerCountryChoice setSelectedSegment:seg];
            [self.providerCountryTextField setHidden:YES];
        }
        [self.providersTable setHidden:YES];
        [self.providerTableScrollView setHidden:YES];
        [self.createNewProviderView setHidden:NO];
        [self.finishEditingButton setHidden:NO];
        [self.providerButtonView setHidden:YES];
        [self.saveNewProviderButton setHidden:YES];
        [self.providerFinishEditing setHidden:NO
         
         
         ];
        [self.wrongArticleGoBackButton setHidden:YES];
        return;
    }
    
    // Article SELECTION
    
    Article *ar = [self selectedArticle];
    if (ar){
        self.articleNameTextField.stringValue = ar.name;
        self.articleIDTextField.integerValue = ar.id;
        self.articlePriceTextView.doubleValue = ar.price;
        [self.articleTable setHidden:YES];
        [self.articleViewTable setHidden:YES];
        [self.createArticleView setHidden:NO];
        //[self.article setHidden:NO];
        [self.articleButtonView setHidden:YES];
        [self.finishEditingArticle setHidden:NO];
        [self.createArticleButton setHidden:YES];
        [self.wrongArticleGoBackButton setHidden:YES];
        return;
    }
}


- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    // Get a new ViewCell
    NSTableCellView *cellView = [tableView makeViewWithIdentifier:tableColumn.identifier owner:self];
    // Since this is a single-column table view, this would not be necessary.
    // But it's a good practice to do it in order by remember it when a table is multicolumn.
    if (tableView == self.clientsTable){
        if( [tableColumn.identifier isEqualToString:@"nameColumn"] )
        {
            Client *client = [self.clients objectAtIndex:row];
            cellView.textField.stringValue = client.name;
            return cellView;
        }
        if( [tableColumn.identifier isEqualToString:@"ageColumn"] )
        {
            Client *client = [self.clients objectAtIndex:row];
            cellView.textField.integerValue = client.age;
            return cellView;
        }
        if( [tableColumn.identifier isEqualToString:@"countryColumn"] )
        {
            Client *client = [self.clients objectAtIndex:row];
            cellView.textField.stringValue = client.country;
            return cellView;
        }
        if( [tableColumn.identifier isEqualToString:@"balanceColumn"] )
        {
            Client *client = [self.clients objectAtIndex:row];
            
            cellView.textField.stringValue = [NSString stringWithFormat:@"%0.2f", client.balance];
            return cellView;
        }
    }
    
    else if (tableView == self.providersTable){
        if( [tableColumn.identifier isEqualToString:@"providerNameColumn"] )
        {
            Provider *provider = [self.providers objectAtIndex:row];
            cellView.textField.stringValue = provider.name;
            return cellView;
        }
        if( [tableColumn.identifier isEqualToString:@"providerCountryColumn"] )
        {
            Provider *provider = [self.providers objectAtIndex:row];
            cellView.textField.stringValue = provider.country;
            return cellView;
        }
    }
    
    else if (tableView == self.articleTable){
        if ([tableColumn.identifier isEqualToString:@"articleNameColumn"]){
            Article *article = [self.articles objectAtIndex:row];
            cellView.textField.stringValue = article.name;
            return cellView;
        }
        if ([tableColumn.identifier isEqualToString:@"articleIDColumn"]){
            Article *article = [self.articles objectAtIndex:row];
            cellView.textField.integerValue = article.id;
            return cellView;
        }
        if ([tableColumn.identifier isEqualToString:@"articlePriceColumn"]){
            Article *article = [self.articles objectAtIndex:row];

            cellView.textField.stringValue = [NSString stringWithFormat:@"%0.2f", article.price];
            return cellView;
        }
    }
    
    return cellView;
}



-(Client*)selectedClient
{
    if (self.clientMenuView.hidden == YES){
        return nil;
    }
    else{
        NSInteger selectedRow = [self.clientsTable selectedRow];
        if( selectedRow >=0 && self.clients.count > selectedRow )
        {        Client *client = [self.clients objectAtIndex:selectedRow];
            return client;
        }
    }
    return nil;
    
}

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    if (tableView== self.clientsTable) {
        return [self.clients count];

    }
    else if (tableView == self.providersTable) {
        return [self.providers count];
    }
    else if (tableView == self.articleTable) {
        return [self.articles count];
    }
    return 0;
}


- (IBAction)goBackAction:(id)sender {
    [self.mainMenuView setHidden: YES];
    [self.clientMenuView setHidden:NO];
    [self.buttonsView setHidden:NO];
    [self.clientsTable setHidden:NO];
    [self.clientsTableView setHidden:NO];
    [self.errorAgeView setHidden:YES];
    [self.clientsTable needsLayout];
    [self.clientsTable reloadData];

}

- (IBAction)onSegmentValueChanged:(id)sender {
    switch ([sender selectedSegment]) {
        case 3:
            [self.clientcountryTextField setHidden:NO];
            break;
            
        default:
            [self.clientcountryTextField setHidden:YES];
            break;
    }
}


- (IBAction)finishEditingAction:(id)sender {
    Client *cl = [self selectedClient];
    if (cl)
    {
        NSString * name = self.clientNameTextField.stringValue;
        NSString * age = self.clientAgeTextField.stringValue;
        NSString* balance = self.balanceTextView.stringValue;
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
        float value = [numberFormatter numberFromString:balance].floatValue;

        if ([self isDecimal:age] && [self isDecimal:balance]){
            
            cl.name = name;
            cl.age = age.integerValue;
            cl.balance = value;
            
        
            if ([self countryChoiceOption].selectedSegment == 0) {
                cl.country = @"Portugal";
            }
            else if([self countryChoiceOption].selectedSegment == 1) {
                cl.country = @"UK";

            }
            else if([self countryChoiceOption].selectedSegment == 2) {
                cl.country = @"USA";

            }
            else if([self countryChoiceOption].selectedSegment == 3) {
                NSString* country = self.clientcountryTextField.stringValue;
                cl.country = country;
            }
            else{
                //nao acontece
            }

            self.clientNameTextField.stringValue = @"";
            self.clientAgeTextField.stringValue = @"";
            self.balanceTextView.stringValue = @"";

            [self.balanceView setHidden:YES];
            [self.createClientView setHidden:YES];
            [self.buttonsView setHidden:NO];
            [self.clientsTableView setHidden:NO];
            
            [self.clientsTable needsLayout];
            [self.clientsTable reloadData];
        }
        else{
            [self.balanceView setHidden:YES];
            [self.createClientView setHidden:YES];
            [self.errorAgeView setHidden:NO];
            [self.clientsTable needsLayout];
            [self.clientsTable reloadData];
        }

    }

}
- (IBAction)exitButtonMainAction:(id)sender {
    exit(0);
}

- (IBAction)clientMenuAction:(id)sender {
    [self.mainMenuView setHidden: YES];
    [self.clientMenuView setHidden:NO];
    [self.buttonsView setHidden:NO];
    [self.clientsTable setHidden:NO];
    [self.clientsTableView setHidden:NO];
}

- (IBAction)articleMenuAction:(id)sender {
    [self.articleMenuView setHidden:NO];
    [self.mainMenuView setHidden:YES];
    [self.createArticleButton setHidden:YES];
    [self.finishEditingArticle setHidden:YES];


}

// PROVIDERS
- (void) initializeProviderList{
    self.providers = [[NSMutableArray alloc] init];
}

- (void) addProviderToList:(Provider*)pr{
    [self.providers addObject:pr];
}
- (IBAction)providerMenuAction:(id)sender {
    [self.providerMenuView setHidden:NO];
    [self.mainMenuView setHidden:YES];
    [self.createNewProviderView setHidden:YES];
}

-(Provider*)selectedProvider
{
    if (self.providerMenuView.hidden == YES){
        return nil;
    }
    else{
        NSInteger selectedRow = [self.providersTable selectedRow];
        if( selectedRow >=0 && self.providers.count > selectedRow )
        {        Provider *provider = [self.providers objectAtIndex:selectedRow];
            return provider;
        }
    }
    return nil;
    
}

- (IBAction)newProviderButton:(id)sender {
    [self.providerTableScrollView setHidden:YES];
    [self.createNewProviderView setHidden:NO];
    [self.providerButtonView setHidden:YES];
    [self.saveNewProviderButton setHidden:NO];
    [self.finishEditingButton setHidden:YES];
    [self.providerFinishEditing setHidden:YES];
    self.providerNameTextFieald.stringValue = @"";
    self.providerCountryTextField.stringValue = @"";


}

- (IBAction)removeProviderButton:(id)sender {
    // 1. Get selected doc
    Provider *provider = [self selectedProvider];
    if (provider)
    {
        // 2. Remove the bug from the model
        [self.providers removeObject:provider];
        // 3. Remove the selected row from the table view.
        [self.providersTable removeRowsAtIndexes:[NSIndexSet indexSetWithIndex:self.providersTable.selectedRow] withAnimation:NSTableViewAnimationSlideRight];
    }
}

- (IBAction)goBackProviderButton:(id)sender {
    [self.providerMenuView setHidden:YES];
    [self.mainMenuView setHidden:NO];
}


- (IBAction)goBackArticleButton:(id)sender {
    [self.articleMenuView setHidden:YES];
    [self.mainMenuView setHidden:NO];
}

- (IBAction)onProviderCountryChoiceSegmentChange:(id)sender {
    switch ([sender selectedSegment]) {
        case 3:
            [self.providerCountryTextField setHidden:NO];
            break;
            
        default:
            [self.providerCountryTextField setHidden:YES];
            break;
    }
}

- (IBAction)goToEditProvider:(id)sender {
    Provider *provider = [self selectedProvider];

    self.providerNameTextFieald.stringValue = provider.name;
    if (provider)
    {
        [self.providerTableScrollView setHidden:YES];
        [self.createNewProviderView setHidden:NO];
        [self.providerButtonView setHidden:YES];
        [self.providerFinishEditing setHidden:NO];
        [self.saveNewProviderButton setHidden:YES];
    }
}

- (IBAction)providerNewSaveButton:(id)sender {
    NSString * name = [self.providerNameTextFieald stringValue];
    Provider *pr;
    if ([self providerCountryChoice].selectedSegment == 0) {
        pr = [[Provider alloc] initWithNameAndCountry: name :@ "Portugal"];
    }
    else if([self providerCountryChoice].selectedSegment == 1) {
        pr = [[Provider alloc] initWithNameAndCountry: name : @"UK"];
    }
    else if([self providerCountryChoice].selectedSegment == 2) {
        pr = [[Provider alloc] initWithNameAndCountry: name : @"USA"];
    }
    else if([self providerCountryChoice].selectedSegment == 3) {
        NSString* country = self.providerCountryTextField.stringValue;
        pr = [[Provider alloc] initWithNameAndCountry: name : country];
    }
    
    [self addProviderToList:pr];
    
    [self.providerTableScrollView setHidden:NO];
    [self.providersTable setHidden:NO];
    [self.createNewProviderView setHidden:YES];
    [self.providerButtonView setHidden:NO];
    
    self.providerNameTextFieald.stringValue = @"";
    self.providerCountryTextField.stringValue = @"";

    [self.providersTable needsLayout];
    [self.providersTable reloadData];
}

- (IBAction)providerNewCancelButton:(id)sender {
    [self.providerTableScrollView setHidden:NO];
    [self.providersTable setHidden:NO];
    [self.createNewProviderView setHidden:YES];
    
    [self.providerButtonView setHidden:NO];
    [self.providerCountryChoice setSelectedSegment:self.segment_ind];

}


- (IBAction)providerFinishEditingButton:(id)sender{
    Provider *pr = [self selectedProvider];
    if (pr)
    {
        NSString * name = self.providerNameTextFieald.stringValue;
        pr.name = name;
        if ([self providerCountryChoice].selectedSegment == 0) {
            pr.country = @"Portugal";
        }
        else if([self providerCountryChoice].selectedSegment == 1) {
            pr.country = @"UK";


        }
        else if([self providerCountryChoice].selectedSegment == 2) {
            pr.country = @"USA";

        }
        else if([self providerCountryChoice].selectedSegment == 3) {
            NSString* country = self.providerCountryTextField.stringValue;
            pr.country = country;
        }
        
        [self.providerCountryChoice setSelectedSegment:self.segment_ind];
        [self.providerTableScrollView setHidden:NO];
        [self.providersTable setHidden:NO];
        [self.createNewProviderView setHidden:YES];
        [self.providerButtonView setHidden:NO];
        
        self.providerNameTextFieald.stringValue = @"";
        self.providerCountryTextField.stringValue = @"";
        
        [self.wrongClientGoBackButton setHidden:NO];
        [self.wrongArticleGoBackButton setHidden:YES];
        
        [self.providersTable needsLayout];
        [self.providersTable reloadData];
    }
}
- (IBAction)newArticleButton:(id)sender {
    [self.createArticleView setHidden:NO];
    [self.articleButtonView setHidden:YES];
    [self.finishEditingArticle setHidden:YES];
    [self.createArticleButton setHidden:NO];
    [self.articleViewTable setHidden:YES];

}

- (IBAction)removeArticleButton:(id)sender {
    Article *article = [self selectedArticle];
    if (article)
    {
        // 2. Remove the bug from the model
        [self.articles removeObject:article];
        // 3. Remove the selected row from the table view.
        [self.articleTable removeRowsAtIndexes:[NSIndexSet indexSetWithIndex:self.articleTable.selectedRow] withAnimation:NSTableViewAnimationSlideRight];
    }
}

- (IBAction)editArticleButton:(id)sender {
    Article *ar = [self selectedArticle];
    if (ar)
    {
        self.articleNameTextField.stringValue = ar.name;
        self.articleIDTextField.integerValue = ar.id;
        self.articlePriceTextView.floatValue = ar.price;
        

        [self.createArticleView setHidden:NO];
        [self.articleButtonView setHidden:YES];
        [self.articleViewTable setHidden:YES];
    }
}

- (IBAction)createArticleButtonAction:(id)sender {
    NSString * name = [self.articleNameTextField stringValue];
    NSString * art_id = self.articleIDTextField.stringValue;
    NSString * price = self.articlePriceTextView.stringValue;

    Article *a;
    if ([self isDecimal:art_id] && [self isDecimal:price]){
        
        a = [[Article alloc] initWithIdNameAndPrice:name :art_id.integerValue :price.floatValue];
        [self addArticleToList:a];
        
        
        [self.articleTable setHidden:NO];
        [self.createArticleView setHidden:YES];
        [self.articleViewTable setHidden:NO];
        [self.articleButtonView setHidden:NO];
        self.articleNameTextField.stringValue = @"";
        self.articleIDTextField.stringValue = @"";
        self.articlePriceTextView.stringValue = @"";
        
        [self.articleTable needsLayout];
        [self.articleTable reloadData];
    }
    else{
        [self.articleMenuView setHidden:YES];
        [self.createArticleView setHidden:YES];
        [self.errorAgeView setHidden:NO];
        [self.wrongClientGoBackButton setHidden:YES];
        [self.wrongArticleGoBackButton setHidden:NO];
        [self.clientsTable needsLayout];
        [self.articleTable reloadData];
    }

}

- (IBAction)articleFinishEditButtonAction:(id)sender {
    NSString * name = [self.articleNameTextField stringValue];
    NSString * art_id = self.articleIDTextField.stringValue;
    NSString * price = self.articlePriceTextView.stringValue;
    
    //convert string to float
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    float value = [numberFormatter numberFromString:price].floatValue;
    
    Article *a = [self selectedArticle];
 
    if ([self isDecimal:art_id] && [self isDecimal:price]){
        
        a.name = name;
        a.id = art_id.integerValue;
        a.price = value;
        
        [self.articleTable setHidden:NO];
        [self.createArticleView setHidden:YES];
        [self.articleViewTable setHidden:NO];
        [self.articleButtonView setHidden:NO];
        self.articleNameTextField.stringValue = @"";
        self.articleIDTextField.stringValue = @"";
        self.articlePriceTextView.stringValue = @"";
        
        [self.articleTable needsLayout];
        [self.articleTable reloadData];
    }
    else{
        [self.articleMenuView setHidden:YES];
        [self.createArticleView setHidden:YES];
        [self.errorAgeView setHidden:NO];
        [self.wrongClientGoBackButton setHidden:YES];
        [self.wrongArticleGoBackButton setHidden:NO];
        [self.clientsTable needsLayout];
        [self.articleTable reloadData];
    }
}

- (IBAction)cancelArticleCreateButtonAction:(id)sender {
    [self.createArticleView setHidden:YES];
    [self.articleButtonView setHidden:NO];
    [self.articleViewTable setHidden:NO];
    [self.articleTable setHidden:NO];
}

- (void) initializeArticleList{
    self.articles = [[NSMutableArray alloc] init];
}
- (void) addArticleToList:(Article*)at{
    [self.articles addObject:at];
}

- (IBAction)wrongArticleGoBack:(id)sender {
    [self.articleMenuView setHidden:NO];
    [self.createArticleView setHidden:NO];
    [self.errorAgeView setHidden:YES];
    [self.clientsTable needsLayout];
    [self.articleTable reloadData];
}

-(Article*)selectedArticle
{
    NSInteger selectedRow = [self.articleTable selectedRow];
    if( selectedRow >=0 && self.articles.count > selectedRow )
    {        Article *article = [self.articles objectAtIndex:selectedRow];
        return article;
    }
    return nil;
    
}

@end
