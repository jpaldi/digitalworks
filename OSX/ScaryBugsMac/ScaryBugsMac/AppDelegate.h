//
//  AppDelegate.h
//  ScaryBugsMac
//
//  Created by jaldeano on 02/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>
@property (weak) IBOutlet NSWindow *window;
@end

