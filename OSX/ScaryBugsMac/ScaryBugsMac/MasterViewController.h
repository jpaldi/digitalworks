//
//  MasterViewController.h
//  ScaryBugsMac
//
//  Created by jaldeano on 02/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "EDStarRating.h"
@interface MasterViewController : NSViewController <NSTableViewDelegate>
@property (strong) NSMutableArray *bugs;

@property (weak) IBOutlet NSTextField *bugTitleView;
@property (weak) IBOutlet NSImageView *bugImageView;
@property (weak) IBOutlet EDStarRating *bugRating;
@property (weak) IBOutlet NSTableView *bugsTableView;
@property (weak) IBOutlet NSButton *deleteButton;
@property (weak) IBOutlet EDStarRating *changePictureButton;

- (IBAction)removeBug:(id)sender;
- (IBAction)starsSelectionChanged:(id)sender;
- (IBAction)bugTitleDidEndEdit:(id)sender;
- (IBAction)changePicture:(id)sender;
@end
