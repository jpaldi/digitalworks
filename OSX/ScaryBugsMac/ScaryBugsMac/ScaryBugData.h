//
//  ScaryBugData.h
//  ScaryBugsMac
//
//  Created by jaldeano on 02/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ScaryBugData : NSObject
@property (strong) NSString *title;
@property (assign) float rating;

- (id)initWithTitle:(NSString*)title rating:(float)rating;
@end
