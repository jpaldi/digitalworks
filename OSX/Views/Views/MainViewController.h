//
//  MainViewController.h
//  Views
//
//  Created by jaldeano on 05/08/16.
//  Copyright © 2016 jaldeano. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "LogViewController.h"
@interface MainViewController : NSViewController
@property (weak) IBOutlet NSButton *imageButton;
@end
