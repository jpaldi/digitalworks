//
//  SharedVars.h
//  Foodit
//
//  Created by DigitalWorks on 5/9/14.
//  Copyright (c) 2014 Fuhao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SharedVars : NSObject

@property (nonatomic) bool loggedIn;
@property (nonatomic) bool printerConnected;
@property (nonatomic) bool isBackground;
@property (nonatomic) NSTimer *backgroundSoundTimer;

+(SharedVars *)sharedVarsObj;

@end
