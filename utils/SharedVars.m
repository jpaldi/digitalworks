//
//  SharedVars.m
//  Foodit
//
//  Created by DigitalWorks on 5/9/14.
//  Copyright (c) 2014 Fuhao. All rights reserved.
//

#import "SharedVars.h"

@implementation SharedVars

+(SharedVars *)sharedVarsObj {
    static SharedVars * single=nil;
    
    @synchronized(self)
    {
        if(!single)
        {
            single = [[SharedVars alloc] init];
        }
    }
    return single;
}

@end
